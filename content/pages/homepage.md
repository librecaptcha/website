Title: Homepage
url:
save_as: index.html
icon: home
start_word: Start using LibreCaptcha
start_sub: Using free and open technology should be available to everyone.
p1_title: Accessibility & private life by-design
p1_icon: universal-access
p1_text: LibreCaptcha has been designed with accessibility and privacy in mind. Webmasters can use it without blocking their users nor offering their datas to some unknown company.
p1_sub: Made to easily replace reCaptcha or run without javascript, LibreCaptcha is balanced between simplicity of changing and simplicity of use.
p2_title: Libre & Self hostable
p2_icon: cog
p2_text: LibreCaptcha is a Free and Libre Software. It's source code can be studied by anybody and/or re-used. Plus, you can host it directly if you want, making it suitable for administrations.
p2_sub: However, we recommend to use only few big instances instead of multiple small ones for efficiency of maintenance and use.
p3_title: Attributed medias
p3_icon: music
p3_text: Each media (images and musics) are shared by artists & authors under a permissive license allowing LibreCaptcha to re-use it after slitghly modifications while generating challenges. LibreCaptcha always display the author and the license (but nor the link!) of the original art.
p3_sub: Images come from <a href="https://commons.wikimedia.org/">Wikimedia Commons</a>, the well known medias stock from Wikimedia Foundation and musics come from <a href="https://dogmazic.net/">Dogmazic</a>, a wonderful french association.
