Title: Hello world!
Date: 2022-05-11 10:20
Category: News
Summary: LibreCaptcha is working hard on releasing a first usable version.

LibreCaptcha is a Captcha engine built to give any developer meaning to break its chains
from Google's reCaptcha.

Main idea is to give access to a Free and Open source Captcha enough strong to block
basic bots (even with audio challenges) while not spying on website nor visitors.
